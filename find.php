<?php
    require_once 'classes/Bot.php';

    if (isset($_POST['find'])) {

        $url = $_POST['find'];

        $bot = new Bot('localhost','root','root','parser');
        $bot->find($url);  
        
    }
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

    <form action="find.php" method="post">
        <input type="text" name="find" class="inp findUrlInp" placeholder="Find">
        <input type="submit" class="btn findBtn" name="submit">
    </form>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="js/index.js"></script>
</body>
</html>