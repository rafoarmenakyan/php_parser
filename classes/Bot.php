<?php

class Bot {

    private $hrefsArr = [];
    private $connection;

    public function __construct ($serverName,$username,$password,$db) {
        $this->serverName = $serverName;
        $this->username = $username;
        $this->password = $password;
        $this->db = $db;

        $this->connection = mysqli_connect($serverName,$username,$password,$db);
    }

    public function __destruct() {
        $this->connection->close();
    }

    private function strip_tags_content($text) {
        return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
    }

    private function add($url, $href, $content, $now) {
        mysqli_query($this->connection, "INSERT INTO links (referrer, href, content, created_at) VALUES ('$url', '$href', '$content', '$now')");
    }

    private function get($ref) {
        return $this->connection->query("SELECT * FROM links WHERE links.referrer = '$ref'");
    }

    private function isInternal($link, $url) {
        $url_parts	= parse_url($url);
        $link_parts	= parse_url($link);

        if (isset($link_parts['host'])) {
            if ($link_parts['host'] == $url_parts['host']) {
                return substr($link, 0, 2) == '//' ? $url_parts['scheme'] . ':' . $link : $link;
            }
        } else if ($link_parts['path'][0] == '/' && strlen($link_parts['path']) > 1) {
            return $url_parts['scheme'] . '://' . $url_parts['host'] . $link;
        }
        return false;
    }

    public function index($url) {

        if (!in_array($url, $this->hrefsArr)) {
            $this->hrefsArr[] = $url;
        }
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);     

        $dom = new DOMDocument;
        libxml_use_internal_errors(true);
    
        @$dom->loadHTML($data, LIBXML_NOWARNING);
        
        foreach ($dom->getElementsByTagName('a') as $link) { 
            $href = $this->isInternal($link->getAttribute('href'), $url);
            $content = $this->strip_tags_content($link->nodeValue);
            if (!in_array($href, $this->hrefsArr) && $href) {
                $this->hrefsArr[] = $href;
                $now = date("Y-m-d H:i:s");
                $this->add($url,$href,$content,$now);
                $this->index($href); 
            } 
        }
    }

    public function find($ref,$num = 0,$level = 0) {

        if ($result = $this->get($ref)) {
            while ($row = $result->fetch_assoc()) {
                echo str_repeat("&nbsp;", $level); 
                echo "<a href='#'>".$row['href']."</a>" . "<br>"; 
                $this->find($row['href'],2 * ($num + 1), $level+1);
            }
        }
   }

}


















