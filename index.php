<?php
    ini_set('max_execution_time', 0);
    ini_set('memory_limit', '512M');
    date_default_timezone_set('Asia/Yerevan');

    require_once 'classes/Bot.php';

    if (isset($_POST['sendUrl'])) {

        $url = $_POST['sendUrl'];

        $bot = new Bot('localhost','root','root','parser');
        $bot->index($url);  
        
        exit;
    }
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

    <input type="text" class="inp sendUrlInp" placeholder="Put Url Here">
    <button class="btn sendBtn">SEND</button>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="js/index.js"></script>
</body>
</html>