const sendInp = document.querySelector('.sendUrlInp');
const sendBtn = document.querySelector('.sendBtn');
const findInp = document.querySelector('.findUrlInp');
const findBtn = document.querySelector('.findBtn');

const send = () => {

    const url = (sendInp.value).trim(); 

    if (!url) {
        alert("Url required");
        return
    }

    $.ajax({
        type: "POST",
        url: "index.php",
        dataType: "json",
        data: {
            sendUrl: url,
        },
    })
}


sendBtn.addEventListener('click', send);

sendInp.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
      send()
    }
});